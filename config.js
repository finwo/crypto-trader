module.exports = {
  authseed : process.env.AUTH_SEED || 'change-me',
  authlen  : 3600,           // Sessions will be active for 5 minutes
};
