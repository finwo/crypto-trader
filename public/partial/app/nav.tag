<template>
  <div class="nav">
    <h5 class="nav-logo"><a href="/">Logo</a></h5>
    <div class="col"></div>
    <a class="nav-item" href="https://github.com/finwo/crypto-trader" target="_blank">Source</a>
    <a class="nav-item" href="#!" onclick="app.logout();">Log out</a>
  </div>
</template>
